﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ImagineRooms.Models
{
    public class RegisterViewModel
    {
        [Required]
        [Display(Name ="Логин")]
        public string Username { get; set; }
        [Required]
        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Некорректный формат Email!!")]
        public string Email { get; set; }
        [Required]
        [Display(Name = "Телефон")]
        [DataType(DataType.PhoneNumber, ErrorMessage = "Некорректный формат номера теефона!")]
        public string Phone { get; set; }
        [Required]
        [Display(Name = "Пароль")]
        [MinLength(8, ErrorMessage = "Пароль слишком короткий!")]
        public string Password { get; set; }
        [Required]
        [Display(Name = "Повтор пароля")]
        [MinLength(8, ErrorMessage = "Пароль слишком короткий!")]
        public string ConfirmPassword { get; set; }
    }

    public class LoginViewModel
    {
        [Required(ErrorMessage = "Введите логин!")]
        [Display(Name = "Логин")]
        public string Username { get; set; }
        [Required(ErrorMessage = "Введите пароль!")]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

    }

    public class AccountViewModel
    {
        public LoginViewModel LoginViewModel;
        public RegisterViewModel RegisterViewModel;
        public RoomSearchViewModel RoomSearchViewModel;
        public RoomCreationViewModel RoomCreationViewModel;
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ImagineRooms.Models
{
    public class RoomCreationViewModel
    {
        [Required]
        [Display(Name="Название комнаты")]
        public string RoomName { get; set; }
        [Display(Name = "Пароль комнаты")]
        public string Password { get; set; }
        [Display(Name = "Проблема(тема)")]
        public string Problem { get; set; }
    }

    public class RoomSearchViewModel
    {
        [Required]
        [Display(Name = "Название комнаты")]
        public string Name { get; set; }
        [Display(Name = "Пароль комнаты")]
        public string Password { get; set; }
    }
}

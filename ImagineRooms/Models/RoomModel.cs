﻿using LiteDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImagineRooms.Models
{
    public class Room
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public User CurrentUser { get; set; }
        public string Password { get; set; }
        public List<RoomMesage> Messages { get; set; }
        public List<User> Users { get; set; }
        public List<Sub> Subs { get; set; }
        public  List<RoomIdea> Ideas { get; set; }
        public string Problem { get; set; }

        public int InactivityTimeout { get; set; }

        public DateTime LastUpdate { get; set; }
    }

    public class Sub
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
    }

    public class RoomIdea
    {
        public int Id { get; set; }
        public int MessageId { get; set; }
        public User Author { get; set; }
        public string Text { get; set; }
    }

    public class RoomMesage
    {
        public int Id { get; set; }
        public User Sender { get; set; }
        public string Text { get; set; }
        public DateTime SendingTime { get; set; }
    }

}

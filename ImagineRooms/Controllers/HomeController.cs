﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ImagineRooms.Models;
using LiteDB;
using ImagineRooms.Extensions;
using Microsoft.AspNetCore.Identity;

namespace ImagineRooms.Controllers
{
    public class HomeController : Controller
    {
        UserManager<User> _userManager;
        public HomeController(UserManager<User> userManager)
        {
            _userManager = userManager;
        }


        public async Task<IActionResult> Index()
        {
            //TODO: Убрать из модели RoomCreationViewModel
            //Свойство CreatorsId, т.к он не нужнен


            return View(new AccountViewModel
            {
                LoginViewModel = new LoginViewModel(),
                RegisterViewModel = new RegisterViewModel(),
                RoomCreationViewModel = new RoomCreationViewModel(),
                RoomSearchViewModel = new RoomSearchViewModel()
            });
        }



        [HttpGet]
        public IActionResult CreateRoom()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> CreateRoom(RoomCreationViewModel viewModel)
        {
            using (var db = new LiteDatabase(@"RoomsDb.db"))
            {
                // Получаем коллекцию
                var col = db.GetCollection<Room>("rooms");

                int lastId = col.GetLastId();

                var newRoom = new Room
                {
                    Id = lastId + 1,
                    Name = viewModel.RoomName,
                    CurrentUser = await User.GetCurrentUser(_userManager),
                    Messages = new List<RoomMesage>(),
                    Password = viewModel.Password,
                };

                col.Insert(newRoom);

                // Индексируем документ по определенному свойству
                col.EnsureIndex(x => x.Id);
            }
            return View();
        }
    }
}

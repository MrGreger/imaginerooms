﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ImagineRooms.Extensions;
using ImagineRooms.Models;
using LiteDB;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Http;

namespace ImagineRooms.Controllers
{
    [Authorize]
    public class RoomsController : Controller
    {

        UserManager<User> _userManager;
        SignInManager<User> _signinManager;
        IHttpContextAccessor _contextAccessor;

        public RoomsController(UserManager<User> userManager, SignInManager<User> signinManager, IHttpContextAccessor contextAccessor)
        {
            _userManager = userManager;
            _signinManager = signinManager;
            _contextAccessor = contextAccessor;
        }

        [HttpGet]
        public async Task<IActionResult> Room(string roomName, string roomPass)
        {

            Room room = null;

            User userToAdd = await User.GetCurrentUser(_userManager);

            if (!CanConnectToRoom(userToAdd.Id, roomName, roomPass))
            {
                return BadRequest();
            }

            using (var db = new LiteDatabase(@"RoomsDb.db"))
            {
                // Получаем коллекцию
                var col = db.GetCollection<Room>("rooms");
                room = col.FindAll().FirstOrDefault(x => x.Name == roomName);
            }

            if (room == null)
            {
                return BadRequest();
            }

            return View(room);
        }

        [HttpPost]
        public async Task<IActionResult> SearchRoom(RoomSearchViewModel model)
        {
            Room r;
            User userToAdd = null;
            using (var db = new LiteDatabase(@"RoomsDb.db"))
            {
                userToAdd = await User.GetCurrentUser(_userManager);
                // Получаем коллекцию
                var col = db.GetCollection<Room>("rooms");
                //Находим комнату
                r = col.FindAll().FirstOrDefault(x => x.Name == model.Name);
                if (r == null)
                {
                    var a = ModelState.TryAddModelError("Name", "Комнаты не существует!");
                    return PartialView("_SearchRoom", model);
                }
            
                if (r.Password != model.Password)
                {
                    var a =  ModelState.TryAddModelError("Password", "Неверный пароль");
                    return PartialView("_SearchRoom", model);
                }
                //Добавляем пользователя в комнату
                if (r.Users.FirstOrDefault(x => x.UserName == User.Identity.Name) == null)
                {
                    r.Users.Add(userToAdd);
                    if (r.Subs.FirstOrDefault(x => x.UserName == User.Identity.Name) == null)
                    {
                        r.Subs.Add(new Sub { Email = userToAdd.Email, UserName = userToAdd.UserName });
                    }
                    col.Update(r);
                }
            }

            if(userToAdd == null)
            {
                return BadRequest();
            }

            //Перенаправляем пользователя в комнату

            string url = Url.Action("Room", "Rooms", new
            {
                //userId = userToAdd.Id,
                roomName = r.Name,
                roomPass = r.Password
            });

            return new JsonResult(new { url = url, success = true });
        }

        [HttpPost]
        public async Task<IActionResult> CreateRoom(RoomCreationViewModel model)
        {
            //Добавляем в кэш новую комнату
            Room r = CreateRoom(model.RoomName, model.Password, model.Problem, await User.GetCurrentUser(_userManager));

            if (r == null)
            {
                var a = ModelState.TryAddModelError("RoomName", "Комната с таким именем уже существует");

                return PartialView("_CreationRoom", model);
            }

            string url = Url.Action("Room", "Rooms", new
            {
                //userId = User.GetUserId(),
                roomName = r.Name,
                roomPass = r.Password
            });

            return new JsonResult(new { url = url, success = true });
        }

        #region Helpers
        public Room CreateRoom(string name, string password,string problem, User creator)
        {
            using (var db = new LiteDatabase(@"RoomsDb.db"))
            {
                // Получаем коллекцию
                var col = db.GetCollection<Room>("rooms");

                int lastId = (int)col.GetLastId();

                if(col.FindAll().FirstOrDefault(x=>x.Name  == name) != null)
                {
                    return null;
                }

                //Создаем экземпляр комнаты
                var newRoom = new Room
                {
                    Id = lastId + 1,
                    Name = name,
                    CurrentUser = creator,
                    Messages = new List<RoomMesage>(),
                    Password = password,
                    Problem = problem,
                    Users = new List<User>
                    {
                        creator
                    },
                    Subs = new List<Sub>
                    {
                        new Sub {Email = creator.Email, UserName = creator.UserName}
                    }
                };


                //Добавляем создателя в комнату

                // Индексируем документ по определенному свойству
                col.EnsureIndex(x => x.Id);

                col.Insert(newRoom);


                return newRoom;
            }
        }
        public bool CanConnectToRoom(string userId, string roomName, string roomPass)
        {

            Room r;
            using (var db = new LiteDatabase(@"RoomsDb.db"))
            {
                // Получаем коллекцию
                var col = db.GetCollection<Room>("rooms");
                //Находим комнату
                r = col.FindAll().FirstOrDefault(x => x.Name == roomName);
                if (r == null)
                {
                    return false;
                }
                //Проверяем пароль

                if (r.Password != roomPass)
                {
                    return false;
                }

                if (r.Users.FirstOrDefault(x=>x.Id == userId) == null)
                {
                    return false;

                }

            }

            return true;
        }
        #endregion

    }
}
﻿using ImagineRooms.Models;
using Microsoft.AspNetCore.Mvc;
using ImagineRooms.Extensions;
using System;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.AspNetCore.Authentication;
using System.Collections.Generic;
using System.Security.Claims;

namespace ImagineRooms.Controllers
{
    public class AccountController : Controller
    {
        UserManager<User> _userManager;
        SignInManager<User> _signInManger;
        ApplicationContext _context;


        public AccountController(UserManager<User> userManager, SignInManager<User> signInManager, ApplicationContext context)
        {
            _userManager = userManager;
            _signInManger = signInManager;
            _context = context;
        }


        [HttpPost]
        public async Task<IActionResult> Register(RegisterViewModel viewModel)
        {
            //Проверка валидности формы
            if (ModelState.IsValid == false)
            {
                return PartialView("_Register", viewModel);
            }

            if (await _userManager.FindByNameAsync(viewModel.Username) != null)
            {
                ModelState.AddModelError("Username", "Данный логин занят!");
                return PartialView("_Register", viewModel);
            }

            if (await _userManager.FindByEmailAsync(viewModel.Email) != null)
            {
                ModelState.AddModelError("Email", "Данный email зарегестрирован!");
                return PartialView("_Register", viewModel);
            }

            //Добавление юзера в бд

            User userToAdd = new User
            {
                Email = viewModel.Email,
                UserName = viewModel.Username,
                PhoneNumber = viewModel.Phone
            };

            await _userManager.CreateAsync(userToAdd, viewModel.Password);
            //  Логин юзера
            await LoginUser(userToAdd, viewModel.Password, true);



            return new JsonResult(true);
        }

        [HttpGet]
        public async Task<IActionResult> Logout()
        {
            await LogoutUser();
            return RedirectToAction("Index", "Home");
        }
        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel viewModel)
        {
            //Проверка валидности формы
            if (ModelState.IsValid == false)
            {
                return PartialView("_Login", viewModel);
            }
            //Поиск юзера в бд
            User user = null;

            user = await _userManager.FindByEmailAsync(viewModel.Username);
            if (user == null)
            {
                user = await _userManager.FindByNameAsync(viewModel.Username);
            }

            if (user == null || !await _userManager.CheckPasswordAsync(user, viewModel.Password))
            {
                //Пользователь не зареган
                ModelState.AddModelError("", "Невереный логин или пароль");
                return PartialView("_Login", viewModel);
            }

            //Логин юзера
            await LoginUser(user, viewModel.Password, true);


            return new JsonResult(true);
        }



        #region Helpers
        //Проверки валидности вводимых в форму данных
        //И вспомогательные методы контроллеров

        async Task<bool> LoginUser(User user, string password, bool persistent)
        {
            //Проверка правильности вводв пароля
            if (!await _userManager.CheckPasswordAsync(user, password))
            {
                return false;
            }



            //Логин юзера
            var r = await _signInManger.PasswordSignInAsync(user, password, persistent, false);
            return true;

        }

        public async Task LogoutUser()
        {
            await _signInManger.SignOutAsync();
        }
        #endregion
    }
}
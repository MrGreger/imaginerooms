﻿using ImagineRooms.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImagineRooms
{
    public class ApplicationContext : IdentityDbContext
    {
        public ApplicationContext(DbContextOptions options)
           : base(options)
        {
            
        }

        public DbSet<Room> Rooms { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Filename=ImagineRoomsDB.db");
        }

        public static ApplicationContext Create()
        {

            //Возвращаем объект контекста данных
            DbContextOptionsBuilder builder = new DbContextOptionsBuilder<ApplicationContext>();
            var o = builder.Options as DbContextOptions<ApplicationContext>;
            return new ApplicationContext(o);
        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            //modelBuilder.Entity<RoomUser>()
            //    .HasKey(x => new { x.RoomId, x.UserId });

            //modelBuilder.Entity<RoomUser>()
            //    .HasOne(ru => ru.Room)
            //    .WithMany(r => r.RoomUsers)
            //    .HasForeignKey(ru => ru.RoomId);

            //modelBuilder.Entity<RoomUser>()
            //    .HasOne(ru => ru.User)
            //    .WithMany(u => u.RoomUsers)
            //    .HasForeignKey(ru => ru.UserId);
                
        }
    }

}

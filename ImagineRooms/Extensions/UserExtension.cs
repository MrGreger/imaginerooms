﻿using ImagineRooms.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ImagineRooms.Extensions
{
    public static class UserExtension
    {
        //Метод-расширение для получения Id юзера
        public static string GetUserId(this ClaimsPrincipal user)
        {
            return ApplicationContext.Create().Users.FirstOrDefault(x=>x.UserName == user.Identity.Name).Id;
        }

        public async static Task<User> GetCurrentUser(this ClaimsPrincipal user,UserManager<User> userManager)
        {
            return await userManager.FindByIdAsync(user.GetUserId());
        }
    }
}

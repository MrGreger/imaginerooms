﻿using ImagineRooms.Models;
using LiteDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImagineRooms.Extensions
{
    public static class LiteDbExtension
    {
        public static int GetLastId(this LiteCollection<Room> col)
        {
            return (int)col.FindAll().Count() > 0 ? (int)col.FindAll().Count() : 0;

        }
    }
}

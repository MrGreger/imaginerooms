﻿using System;
using System.Linq;
using System.Net;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using static System.Console;

namespace ImagineRooms
{
    public class Program
    {
       public static string LISTEN_IP { get; private set; } = null;
        public static void Main(string[] args)
        {
            String strHostName = string.Empty;
            strHostName = Dns.GetHostName();
            IPHostEntry ipEntry = Dns.GetHostEntry(strHostName);
            IPAddress[] addr = ipEntry.AddressList;
            //_listenIP = addr[2].ToString();
            LISTEN_IP = addr.FirstOrDefault(x => x.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork).ToString();
            if (string.IsNullOrWhiteSpace(LISTEN_IP))
            {
                LISTEN_IP = "127.0.0.1";
            }

            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .UseKestrel(options =>
                {
                 options.Limits.MaxConcurrentConnections = 100;
                 options.Limits.MaxRequestBodySize = 10 * 1024;
                 options.Limits.MinRequestBodyDataRate =
                     new MinDataRate(bytesPerSecond: 100, gracePeriod: TimeSpan.FromSeconds(10));
                 options.Limits.MinResponseDataRate =
                     new MinDataRate(bytesPerSecond: 100, gracePeriod: TimeSpan.FromSeconds(10));
                 options.Listen(IPAddress.Parse(LISTEN_IP), port: 5000);
                 })
                .Build();
    }
}

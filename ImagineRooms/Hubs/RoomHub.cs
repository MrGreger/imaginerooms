﻿using ImagineRooms.Extensions;
using ImagineRooms.Models;
using LiteDB;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Concurrent;

namespace ImagineRooms.Hubs
{
    //  [Authorize]
    public class RoomHub : Hub
    {
        UserManager<User> _userManager;
        SignInManager<User> _signinManager;
        private static List<Room> _rooms;

        public RoomHub(UserManager<User> userManager, SignInManager<User> signInManager)
        {
            _signinManager = signInManager;
            _userManager = userManager;

            if (_rooms == null)
            {
                _rooms = new List<Room>();
            }

        }

        public async Task Send(string message, string roomId)
        {
            User sender = await Context.User.GetCurrentUser(_userManager);
            await SendUserMessage(sender, message, roomId);
        }

        //Подключение к комнате
        public async Task Join(string roomId)
        {

            await Groups.AddAsync(Context.ConnectionId, roomId);
            Room curRoom = null;



            if (_rooms.FirstOrDefault(x => x.Id.ToString() == roomId) == null)
            {
                using (var db = new LiteDatabase(@"RoomsDb.db"))
                {
                    // Получаем коллекцию
                    var col = db.GetCollection<Room>("rooms");

                    curRoom = col.FindAll().FirstOrDefault(x => x.Id.ToString() == roomId);

                    _rooms.Add(curRoom);
                }
            }
            else
            {
                curRoom = _rooms.FirstOrDefault(x => x.Id.ToString() == roomId);
            }

            if (curRoom == null)
            {
                return;
            }

            //Выводим для нового пользователя все сообщения
            for (int i = 0; i < curRoom.Messages.Count; i++)
            {
                if (curRoom.Messages[i].SendingTime != null && curRoom.Messages[i].Sender != null)
                {
                    await Clients.Client(Context.ConnectionId)
                          .InvokeAsync("Send", new
                          {
                              msg = $"[{curRoom.Messages[i].SendingTime.ToShortTimeString()}] {curRoom.Messages[i].Sender.UserName} : {curRoom.Messages[i].Text}",
                              msgId = curRoom.Messages[i].Id
                          });
                }
                else
                {
                    await Clients.Client(Context.ConnectionId)
                                                  .InvokeAsync("Send", new { msg = $"{curRoom.Messages[i].Text}" });
                }
            }

            string msgText = $"[{DateTime.Now.ToShortTimeString()}] пользователь {Context.User.Identity.Name} подключился.";
            await SendServerMessage(msgText, roomId);
            await Clients.Group(roomId).InvokeAsync("OnConnected", new { room = curRoom });

        }

        //отключение от комнаты
        public async Task Disconnect(string roomId)
        {


            string msgText = $"[{DateTime.Now.ToShortTimeString()}] пользователь {Context.User.Identity.Name} отключился.";

            await SendServerMessage(msgText, roomId);

            EmailService emailService = new EmailService();

            await Groups.RemoveAsync(Context.ConnectionId, roomId);

            using (var db = new LiteDatabase(@"RoomsDb.db"))
            {
                var roomCollection = db.GetCollection<Room>("rooms");

                var rooms = roomCollection.FindAll();
                var curRoom = rooms.FirstOrDefault(x => x.Id.ToString() == roomId);
                if (curRoom != null)
                {
                    User u = await _userManager.FindByNameAsync(Context.User.Identity.Name);
                    User userToDelete = curRoom.Users.FirstOrDefault(x => x.Id == u.Id);
                    if (userToDelete != null)
                    {
                        curRoom.Users.Remove(userToDelete);
                        roomCollection.Update(curRoom);
                    }
                }
                if (curRoom.Ideas != null)
                {
                    if (curRoom.Users.Count == 0 && curRoom.Ideas.Count >= 0)
                    {
                        string statistics = "";
                        foreach (RoomIdea idea in curRoom.Ideas)
                        {
                            statistics += idea.Text + "   (" + idea.Author.UserName + ")<br/>\n";
                        }
                        foreach (Sub u in curRoom.Subs)
                        {
                            await emailService.SendEmailAsync(u.Email, "Ваши идеи", statistics);
                        }
                        roomCollection.Delete(x => x.Id == curRoom.Id);
                    }
                }

               
            }
        }

        public async Task ToggleIdea(string roomId, int messageId)
        {
            using (var db = new LiteDatabase(@"RoomsDb.db"))
            {
                var roomCollection = db.GetCollection<Room>("rooms");

                var rooms = roomCollection.FindAll();
                var room = rooms.FirstOrDefault(x => x.Id.ToString() == roomId);

                if (room == null)
                {
                    return;
                }


                RoomMesage message = room.Messages.FirstOrDefault(x => x.Id == messageId);

                if (message == null)
                {
                    return;
                }
                //Если данного сообщения еще нет в идеях
                if (room.Ideas == null)
                {
                    room.Ideas = new List<RoomIdea>();
                }

                if (room.Ideas.FirstOrDefault(x => x.MessageId == messageId) == null)
                {
                    RoomIdea idea = new RoomIdea
                    {
                        Id = room.Ideas.Count != 0 ? room.Ideas.Last().Id + 1 : 0,
                        Author = message.Sender,
                        MessageId = message.Id,
                        Text = message.Text
                    };

                    room.Ideas.Add(idea);
                }
                else
                {
                    room.Ideas.Remove(room.Ideas.FirstOrDefault(x => x.MessageId == message.Id));
                }



                roomCollection.Update(room);
                await Clients.Group(roomId).InvokeAsync("IdeaToggled", new { msg = message, userId = message.Sender.Id });
            }


        }

        async Task SendServerMessage(string text, string roomId)
        {
            Room curRoom = null;
            using (var db = new LiteDatabase(@"RoomsDb.db"))
            {
                // Получаем коллекцию
                var col = db.GetCollection<Room>("rooms");

                curRoom = col.FindAll().FirstOrDefault(x => x.Id.ToString() == roomId);

                User user = curRoom.Users.FirstOrDefault(x => x.UserName == Context.User.Identity.Name);

                RoomMesage msg = new RoomMesage
                {
                    Text = text,
                    Id = curRoom.Messages.Count != 0 ? curRoom.Messages.Last().Id + 1 : 0

                };
                //Добавляем сообщение в бд
                curRoom.Messages.Add(msg);
                await Clients.Group(roomId).InvokeAsync("Send", new { msg = msg.Text });
                col.Update(curRoom);
            }
        }

        async Task SendUserMessage(User sender, string text, string roomId)
        {
            Room curRoom = null;
            using (var db = new LiteDatabase(@"RoomsDb.db"))
            {
                // Получаем коллекцию
                var col = db.GetCollection<Room>("rooms");

                //Добавляем сообщение в бд
                curRoom = col.FindAll().FirstOrDefault(x => x.Id.ToString() == roomId);
                RoomMesage msg = new RoomMesage
                {
                    Sender = sender,
                    SendingTime = DateTime.Now,
                    Text = text,
                    Id = curRoom.Messages.Count != 0 ? curRoom.Messages.Last().Id + 1 : 0

                };

                curRoom.Messages.Add(msg);
                await Clients.Group(roomId).InvokeAsync("Send", new { msg = $"[{msg.SendingTime.ToShortTimeString()}] { msg.Sender.UserName}: { msg.Text }", msgId = msg.Id });

                col.Update(curRoom);
            }
        }

        public static void StartTimeoutHandler()
        {
            Thread timeoutTask = new Thread(new ThreadStart(() =>
            {
                while (true)
                {
                    Room[] rooms;
                    using (var db = new LiteDatabase(@"RoomsDb.db"))
                    {
                        // Получаем коллекцию
                        var col = db.GetCollection<Room>("rooms");

                        rooms = col.FindAll().ToArray();
                    }
                    if (rooms.Length > 0)
                    {
                        //Проверка таймауа и пинг каждой комнаты
                    }
                    else
                    {
                        //Console.WriteLine("Комнат нет");
                        Thread.Sleep(1000);
                    }
                }
            }));

            timeoutTask.Start();
        }

        public void StartNewRoom(Room room)
        {
            Thread newRoomThread = new Thread(new ThreadStart(() =>
            {
                TimeSpan time;
                do
                {

                    time = DateTime.Now - room.LastUpdate;
                } while (time.Seconds <= 60);
            }));
        }
    }
}

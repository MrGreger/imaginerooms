﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ImagineRooms.Models;
using ImagineRooms.Hubs;
using Microsoft.AspNetCore.Http;

namespace ImagineRooms
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddEntityFrameworkSqlite();
            services.AddSignalR();
            services.AddDbContext<ApplicationContext>();

            services.AddIdentity<User, IdentityRole>(
               opts =>
               {
                   opts.Password.RequiredLength = 8;   // минимальная длина
                   opts.Password.RequireNonAlphanumeric = false;   // требуются ли не алфавитно-цифровые символы
                   opts.Password.RequireLowercase = false; // требуются ли символы в нижнем регистре
                   opts.Password.RequireUppercase = false; // требуются ли символы в верхнем регистре
                   opts.Password.RequireDigit = false; // требуются ли цифры
               }
               )
               .AddDefaultTokenProviders()
               .AddEntityFrameworkStores<ApplicationContext>();

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            using (ApplicationContext db = ApplicationContext.Create())
            {
                db.Database.EnsureCreated();
            }

            RoomHub.StartTimeoutHandler();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();
            

            app.UseAuthentication();

            //Добавляем хаб для комнат
            app.UseSignalR(
                routes =>
                {
                    routes.MapHub<RoomHub>("room");
                });

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
